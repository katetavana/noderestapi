var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var url = require ('url');

var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

var analyse = require ('./modules/analyserequest');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



/*
 * To test json data, initially we  read it from a file.
 * The following Get request returns entire response.
 *  
	app.get('/', function(request, response) {
		console.log ('request received by get ...');
		response.setHeader('content-type','application/json');
	    response.end(JSON.stringify (analyse.get_drm_episode(request,response)));
	 });
 */

app.get('/', function(request, response) {
	console.log ('request received by get ...');
	response.setHeader('content-type','application/json');
	response.status(404).json({error: "There is no such URL"});
    response.end(JSON.stringify (response.status(404).json({error: "There is no such URL"})));
 });

app.post('/data', function(request, response) {
	console.log ('request received by post ...');
	response.setHeader('content-type','application/json');
	response.end(JSON.stringify (analyse.post_drm_episode(request,response)));
 });


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	console.log ("There is an error ...");
	res.locals.message = err.message;
	//res.locals.error = req.app.get('env') === 'development' ? err : {};
	res.locals.error ={};
	if(err.status == 400)
	{
		res.status(400).json({error: "Could not decode request: JSON parsing failed"});
	}else{
		res.status(err.status || 500);
		res.render('error');
	}
  
});

module.exports = app;

var listener = app.listen(8081, function(){
    console.log('Listening on port ' + listener.address().port); 
});

