var fs = require('fs');

function read_json_file() 
{
	var file = './data/images.json';
   return fs.readFileSync(file); 
}  

/*exports.list = function() {
   return JSON.parse(read_json_file());
}
*/

exports.get_drm_episode = function(req, response) {
	var json_result = JSON.parse(read_json_file());
	var payload = json_result.payload;
	var resultArray = new Array ();
	 
	 for (var i = 0; i < payload.length; i++) 
	 {
		 console.log ('I: '+i);
	   var drm = payload[i].drm;
	   var episodeCount = payload[i].episodeCount;
	   if (drm == true && episodeCount > 0)
	   {
		   var showImage = payload[i].image.showImage;
		   var slug = payload[i].slug;
		   var title= payload[i].title;
		   var response = { image:showImage , slug: slug, title:title }
		    
		   console.log ('building a response title : '+response.title);  
		   resultArray.push(response);
	   
	   }
	}
	 var out = {"response":resultArray}
	   return out;   
}



exports.post_drm_episode = function(req, response) {
	try{
		var payload = req.body.payload;
		var resultArray = new Array ();
		
		if(payload != null && payload.length > 0)
		{
			console.log ('request body length is: '+payload.length); 
			for (var i = 0; i < payload.length; i++) 
			 {
			    var drm = payload[i].drm;
			   var episodeCount = payload[i].episodeCount;
			   if (drm == true && episodeCount > 0)
			   {
				   console.log ('Iteration is: '+i);
				   var showImage = payload[i].image.showImage;
				   var slug = payload[i].slug;
				   var title= payload[i].title;
				   var response = { image:showImage , slug: slug, title:title };
				   console.log ('building a response title : '+response.title);  
				   resultArray.push(response);
			   }
			}
			 var out = {"response":resultArray};
			 return out;   
		}else{
			 console.log ('json request lenght is less than one, undefined or invalid jason >>> ERROR ...');
			 response.status(400).json({error: "Could not decode request: JSON parsing failed"});
		}
	}catch(error){
		console.log(error);
	}
	   
}